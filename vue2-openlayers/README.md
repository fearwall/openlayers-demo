# vue-openlayers

[🎈点击预览本项目](http://k21vin.gitee.io/vue-openlayers/)
<br>
:rocket: [ol在vue3中运行，仓库地址](https://gitee.com/k21vin/front-end-data-visualization)
<br>
 :rocket: [ol在vue3中运行，预览地址  :rocket:  :rocket:  :rocket:  :rocket: ](http://k21vin.gitee.io/front-end-data-visualization/#/openlayers/ol-basic/ol-stated)

在 ``Vue2`` 上使用 ``openlayers``。
本项目为练习案例。

![微信订阅号：Rabbit_svip](https://images.gitee.com/uploads/images/2020/0606/204201_c329f5ac_4809606.png)


<br>

## 安装
```
$ npm install
```
或者
```
$ npm install --registry=https://registry.npm.taobao.org
```

<br>

### 运行
```
$ npm run serve
```

<br>

### 打包
```
$ npm run build
```

<br>

### 说明
使用了以下框架和库。<br>
- [Vue 文档](https://cn.vuejs.org/)
- [Vue-Router 文档](https://router.vuejs.org/zh/)
- [openlayers 官网](https://openlayers.org/)


# 更多推荐
**openlayers 在 Vue3 里应用**

 :rocket: [ol在vue3中运行，仓库地址](https://gitee.com/k21vin/front-end-data-visualization)
<br>
 :rocket: [ol在vue3中运行，预览地址](http://k21vin.gitee.io/front-end-data-visualization/#/openlayers/ol-basic/ol-stated)

<br>

<br>

# 关于我

雷猴，我是德育处主任，一个喜欢吃喝玩乐的地球生物。只要和工作无关的事情我都觉得很有趣。

<br>

我正在整理前端可视化相关库的笔记，不定期在 [掘金](https://juejin.cn/column/7050370347324932132)、[CSDN](https://blog.csdn.net/weixin_39415598)、[思否](https://segmentfault.com/) 更新。

如果方便的话，可以点进我的 [掘金主页 **帮我的文章点个赞~**](https://juejin.cn/user/2673620576140030/posts) ，或者点其他平台也行。感恩🌷

<br>

已经停更了很久的 **订阅号（德育处主任）** 也有计划重启了，我打算把每个我学过的库整理成独立的专栏，以循序渐进的方式和大家一起摄入知识~

![公众号：德育处主任](./OfficialAccounts.gif)

<br>

> 最后最后，如果你觉得我的文章对你有帮助，可以请我的猫吃个罐头~
>
> ![公众号：德育处主任](./q.png)